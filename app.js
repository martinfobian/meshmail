"use strict";

var Network = require('./src/Network'),
    Contact = require('./src/Contact'),
//    NetworkStorage = require('./src/NetworkStorage'),
    colors = require('colors'),
    Q = require('q'),
    nopt = require('nopt'),
    _ = require('underscore'),
    forge = require('node-forge'),
    webUI = require('./src/webUI');

var MeshMail = require('./src/MeshMail');

MeshMail.start(0, true).then(function() {


    var webPort = webUI.start(0);
    if (getParameters('web')) {
        var open = require('open');
        open('http://localhost:' + webPort + '');
    }

    console.log("WebUI listening on port", webPort);
});



//var port = 2109;
//
//if (getParameters('web')) {
//    webUI.start(port);
//    var open = require('open');
//    open('http://localhost:' + port + '');
//}
//
//
////Storage.storeValue('12f0f45747e6e110172fa042bc619ff6b7039c4e', { city: 'Berlin' });
//var N = Network.start(getParameters('port')).then(function (port) {
//
//    console.log("Listening on port", port);
//
//    var bootstrapContact = getBootstrapContact();
//    console.log("Your NodeID is", Network._getHomeContact().getNodeId().yellow);
//
//    if (bootstrapContact) {
//        console.log("Bootstrapping from", bootstrapContact.getIp(), bootstrapContact.getPort(), "...");
//
//
//
//        Network._sendRPC(
//            bootstrapContact,
//            'FIND_NODE',
//            { id: Network._getHomeContact().getNodeId() }
//        ).then(function (response) {
//
//            var md = forge.md.sha1.create();
//            md.update('mail@martinfobian.de-credentials');
//            console.log("Credentials", md.digest().toHex());
//
//            var cred = md.digest().toHex();
//
//            Network.storeValue(cred, { city: 'Denver'});
//            console.log("Waiting 5s");
//            setTimeout(function(){
//
//                Network.findValue(cred).then(function (res) {
//                    console.log(res);
//                }, function (err) {
//                    console.log("ERROR", "Couldn't find value.");
//                });
//
//            }, 5000);
//
//
//
//
////            Network.findClosestNodes(cred).then(function (res) {
////                _.each(res, function (node) {
////                    console.log(node.nodeId, require('./src/Routing').distance(cred, node.nodeId));
////                });
////            });
//
//        }, function (err) {
//            console.log("FAILED! Node unreachable.");
//            process.exit(-1);
//        });
//    }
//});

///**
// * Parse command line options and return a NodeHandle to bootstrap from if possible
// *
// * @returns {*}
// */
//function getBootstrapContact() {
//
//    var parameters = getParameters();
//    if (!parameters['bootstrap']) {
//        return null;
//    }
//
//    if (parameters['bootstrap'].indexOf(':') > 0) {
//        parameters = parameters['bootstrap'].split(':')
//        return Contact.fromIPAndPort.apply(null, parameters);
//    } else {
//        return null;
//    }
//}

function getParameters(key) {

    var knownOpts = {
//            "port":[Number, null],
//            "bootstrap": [String, null],
            "web": [Boolean, true]
        },
        shortHands = {
//            "p": ["--port"],
//            "bs": ["--bootstrap"]

        };

    var options = nopt(knownOpts, shortHands, process.argv, 2);
    if (key) {
        return options[key];
    } else {
        return options;
    }
}

//function getPort() {
//    var knownOpts = {
//        "port":[Number, null]
//    },
//    shortHands = {
//        "p": ["--port"]
//    },
//    parameters = nopt(knownOpts, shortHands, process.argv, 2);
//    if (!parameters['port']) {
//        return null;
//    }
//    return parameters['port'];
//}

Object.prototype.getName = function() {
    var funcNameRegex = /function (.{1,})\(/;
    var results = (funcNameRegex).exec((this).constructor.toString());
    return (results && results.length > 1) ? results[1] : "";
};

Date.prototype.timeNow = function(){
    return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
};

Array.prototype.containsID = function (item) {
    var index = this.indexOf(item);
    if (index > -1) {
        return true;
    } else {
        return false;
    }
}

Array.prototype.pushUnique = function (item){

    for (var i = 0; i < this.length; i++) {
        if (JSON.stringify(this[i]) == JSON.stringify(item)) {
            return this;
        }
    }
    this.push(item);
    return this;
}

Array.prototype.removeIDIfExists = function (item) {
    var index = this.indexOf(item);
    if (index > -1) {
        this.splice(index, 1);
    }
}

Array.prototype.removeContactIfExists = function (nodeID) {

    for (var i = 0; i < this.length; i++) {
        if (this[i].nodeId === nodeID) {
            this.splice(i, 1);
            return;
        }
    }
}


