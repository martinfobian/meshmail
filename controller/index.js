var webUI = require('../src/webUI'),
    Network = require('../src/Network'),
    Util = require('../src/Util');


module.exports = function (app) {

    app.get('/splash', function (req, res, next) {
        res.render('splash');
    });

    app.get('/mail',
        webUI.requiredBootstrapping,
        webUI.requiredAuthentication,
        function (req, res, next) {
            webUI.meshMail.getMail(req.query.id).then(function (mail) {
                mail = mail || {};
                res.render('mail', { mail: mail });
            });
        }
    );

    app.post('/mail',
        webUI.requiredBootstrapping,
        webUI.requiredAuthentication,
        function (req, res, next) {
            webUI.meshMail.getMail(req.body.id).then(function (mail) {
                mail = mail || {};
               res.json(mail);
            });
        });

    app.get('/',
        webUI.requiredBootstrapping,
        webUI.requiredAuthentication,
        function (req, res, next) {
            webUI.meshMail.getInbox(req.user.username).then(function (inbox) {

                console.log("IN", inbox);

                inbox = inbox || [];

//                res.json({ a : inbox });

                res.render('index', {
                    user: req.user,
                    mails: inbox,
                    mail_count: inbox.length
                });
            });
        });

    app.get('/compose',
        webUI.requiredBootstrapping,
        webUI.requiredAuthentication,
        function (req, res, next) {
            res.render('compose');
        });

    app.post('/compose',
        webUI.requiredBootstrapping,
        webUI.requiredAuthentication,
        function (req, res, next) {

            var messageKey = Util.randomIDSync();
            var message = {
                body: req.body.body,
                to: req.body.recipient,
                from: req.user.username,
                subject: req.body.subject,
                date: require('moment')().format()
            };

            Network.storeValue(messageKey, message);
            var inboxKey = Util.getHash('inbox-' + req.body.recipient);
            Network.appendValue(inboxKey, messageKey);

            req.flash('success', 'Message sent to ' + req.body.recipient);
            res.redirect('/');
        });
}