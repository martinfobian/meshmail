var passport = require('passport'),
    webUI = require('../src/webUI'),
    MeshMail = require('../src/MeshMail');

module.exports = function(app) {

    app.get('/login',
        webUI.requiredBootstrapping,
        function (req, res, next) {
            res.render('login');
        }
    );

    app.get('/settings',
        webUI.requiredBootstrapping,
        function (req, res, next) {
            res.render('settings');
        }
    );

    app.get('/bootstrap', function (req, res, next) {

        if (MeshMail.isBootstrapped) {
            return res.redirect('/');
        }

        MeshMail.getLastSuccessfulBootstrapNode().then(function (lastBootstrap) {
            req.flash('info', 'Your MeshMail-Client has not yet joined the network. Please enter the node details you want to use for bootstrapping.');
            return res.render('bootstrap', { lastBootstrap: lastBootstrap });
        });
    });

    app.post('/bootstrap', function (req, res, next) {

        var pattern = /\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\:([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])\b/;
        if (!pattern.test(req.body.ip_port)) {
            req.flash('error', 'Invalid IP address, port combination');
            return res.render('bootstrap');
        }

        var ip = req.body.ip_port.split(':')[0];
        var port = req.body.ip_port.split(':')[1];

//        req.flash('info', 'Bootstrapping from ' + ip + ':' + port);

        MeshMail.bootstrap(ip, port).then(function (result) {

            return res.redirect('/login');

        }, function (err) {

            req.flash('error', 'Bootstrapping failed. ' + err);
            return res.render('bootstrap');
        });
    });

    app.post('/login',
        webUI.requiredBootstrapping,
        passport.authenticate('local',
            {
                successRedirect: '/',
                failureRedirect: '/login',
                failureFlash: true
            }
        ));

    app.get('/register',
        webUI.requiredBootstrapping,
        function (req, res, next) {
            res.render('register');
        }
    );

    app.post('/register',
        webUI.requiredBootstrapping,
        function (req, res, next) {

            if (req.body.password != req.body.password_repeat) {
                req.flash('error', 'Passwords don\'t match!');
                return res.render('register');
            }

            MeshMail.register(req.body.username, req.body.password).then(function () {

                req.flash('success', "Account " + req.body.username + " created!");
                return res.render('register');

            }, function (err) {
                console.log(err);
                req.flash('error', 'Account could not be created. Might already be taken.');
                return res.render('register');

            });
        }
    );

    app.get('/logout', function (req, res) {
        req.session.destroy(function () {
            res.redirect('/');
        });
    });
}