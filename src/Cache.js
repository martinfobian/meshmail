"use strict";

var levelup = require('levelup'),
    Util = require('./Util');

var db = levelup('/cache-' + Util.randomIDSync(), { db: require('memdown'), valueEncoding: 'json' }),
    Q = require('q');

var Cache = {

    getValue: function (hash) {

        var deferred = Q.defer();

        db.get(hash, function (err, value) {

            if (err.notFound) {
                deferred.resolve(null);
                return;
            }

            if (err)  {
                deferred.reject(err);
            } else {
                deferred.resolve(value);
            }
        });

        return deferred.promise;
    },

    storeValue: function (hash, value) {

        var deferred = Q.defer();

        db.put(hash, value, function (err) {

            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve({});
            }
        });

        return deferred.promise;
    },

    deleteValue: function (hash) {

        var deferred = Q.defer();

        db.del(hash, function (err) {

            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve({});
            }
        });

        return deferred.promise;
    }
}

module.exports = Cache;