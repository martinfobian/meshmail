"use strict";

var Q = require('q'),
    forge = require('node-forge');

function Contact(nodeId, ip, port) {
    this.nodeId = nodeId;
    this.ip = ip;
    this.port = port;
}

Contact.fromIPAndPort = function (ip, port) {
    return new Contact(null, ip, parseInt(port));
}

Contact.create = function() {

    var bytes = forge.random.getBytesSync(20);
    var nodeId = forge.util.bytesToHex(bytes);

    return new Contact(nodeId, null, null);
}

Contact.prototype.getNodeId = function() {
    return this.nodeId;
}

Contact.prototype.getIp = function() {
    return this.ip;
}

Contact.prototype.setIp = function(ip) {
    this.ip = ip;
}

Contact.prototype.getPort = function() {
    return this.port;
}

Contact.prototype.setPort = function(port) {
    this.port = port;
}

module.exports = Contact;