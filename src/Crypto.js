var forge = require('node-forge');

var Crypto = {

    IV: 'w4VKw5nCumbDjcKZw4NBYTVyw5bCiUXCpQ==',

    encryptJSON: function (json, key) {

        var text = JSON.stringify(json);
        var cipher = forge.aes.createEncryptionCipher(key, 'CBC');
        var iv = new Buffer(Crypto.IV, 'base64').toString();

        cipher.start(iv);
        cipher.update(forge.util.createBuffer(text, 'utf8'));
        cipher.finish();

        return cipher.output;
    },

    decryptJSON: function (encrypted, key) {

        var cipher = forge.aes.createDecryptionCipher(key, 'CBC');
        var iv = new Buffer(Crypto.IV, 'base64').toString();

        cipher.start(iv);
        cipher.update(encrypted);
        cipher.finish();

        return JSON.parse(cipher.output);
    }
}

module.exports = Crypto;