"use strict";

var BUCKET_SIZE = 20;
var _ = require('underscore');

/**
 * Constructor
 * @param rangeMin
 * @param rangeMax
 * @constructor
 */
function Bucket(rangeMin, rangeMax, bucketSize) {

    this._bucketSize = bucketSize || BUCKET_SIZE;

    this._rangeMin = rangeMin;
    this._rangeMax = rangeMax;
    this._contacts = [];
}

/**
 * Add the given contact to this bucket
 * @param contact
 */
Bucket.prototype.addContact = function (contact) {
    if (this.containsContact(contact)) {

        this.removeContact(contact);
        this._contacts.unshift(contact);

    } else if (this._contacts.length < this._bucketSize) {

        this._contacts.unshift(contact);

    } else {
        throw new Error('Bucket full');
    }
}

Bucket.prototype.getRandomID = function () {
    return _.random(this._rangeMin, this._rangeMax - 1).toString(16);
}

/**
 * Remove the given contact from this bucket
 * @param contact
 */
Bucket.prototype.removeContact = function (contact) {

    var key, index = -1;

    for (key in this._contacts) {
        if (JSON.stringify(this._contacts[key]) === JSON.stringify(contact)) {
            index = key;
        }
    }

    if (index >= 0) {
        this._contacts.splice(index, 1);
    }
}

/**
 * Get the first (freshest) contact
 * @returns {*}
 */
Bucket.prototype.getFresh = function () {
    return this._contacts[0];
}

/**
 * Get first (freshest) contact and remove it
 * @returns {*}
 */
Bucket.prototype.popFresh = function () {
    return this._contacts.shift();
}

Bucket.prototype.getOld = function () {
    return this._contacts[this._contacts.length - 1];
}

Bucket.prototype.popOld = function () {
    return this._contacts.pop();
}

/**
 * Check whether the given contact is in this bucket
 * @param contact
 * @returns {boolean}
 */
Bucket.prototype.containsContact = function (contact) {

    var key;

    for (key in this._contacts) {
        if (JSON.stringify(this._contacts[key]) === JSON.stringify(contact)) {
            return true;
        }
    }

    return false;
}

/**
 * Get the contact with the given NodeID
 * @param nodeId
 * @returns {*}
 */
Bucket.prototype.getContact = function (nodeId) {

    var result = null;

    this._contacts.some(function (contact) {
       if (contact.getNodeId() === nodeId) {
           result = contact;
           return true;
       }
    });

    return result;
}

/**
 * Return all contacts
 * @returns {Array}
 */
Bucket.prototype.getContacts = function () {
    return this._contacts;
}

/**
 * Check if the given key belongs in this bucket
 * @param key
 * @returns {boolean}
 */
Bucket.prototype.isInRange = function (key) {
    var keyInt = parseInt(key, 16);
    return this._rangeMin <= keyInt && keyInt < this._rangeMax;
}

Bucket.prototype.touch = function (findNodeFunction) {
    findNodeFunction(this.getRandomID());
}

module.exports = Bucket;