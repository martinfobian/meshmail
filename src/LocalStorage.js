"use strict";

var levelup = require('levelup'),
    db = levelup('./data/mesh', { valueEncoding: 'json' }),
//    db = levelup('/mesh', { db: require('memdown'), valueEncoding: 'json' }),
    Q = require('q');

var LocalStorage = {

    db: {},

    getValue: function (hash) {

        var deferred = Q.defer();

        db.get(hash, function (err, value) {

            if (err)  {
                deferred.reject(err);
            } else {
                deferred.resolve(value);
            }
        });

        return deferred.promise;
    },

    storeValue: function (hash, value) {

        var deferred = Q.defer();

        db.put(hash, value, function (err) {

            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve({});
            }
        });

        return deferred.promise;
    }
}

module.exports = LocalStorage;