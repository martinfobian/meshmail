"use strict";

var PublicProfile = require('./MeshMail/PublicProfile'),
    PrivateProfile = require('./MeshMail/PrivateProfile'),
    NetworkStorage = require('./NetworkStorage'),
    Contact = require('./Contact'),
    Network = require('./Network'),
    forge = require('node-forge'),
    Q = require('q'),
    webUI = require('./webUI'),
    Util = require('./Util');

var MeshMail = {

    getLastSuccessfulBootstrapNode: function () {
        var deferred = Q.defer();

        require('fs').readFile(__dirname + '/../.lastBootstrap', {encoding: 'utf-8'}, function(err, bootstrap){
            if (!err){
                deferred.resolve(bootstrap);
            } else {
                deferred.resolve('');
            }
        });

        return deferred.promise;
    },

    bootstrap: function (ip, port) {

        var deferred = Q.defer();

        var bootstrapContact = Contact.fromIPAndPort(ip, port);
        console.log("Bootstrapping from", bootstrapContact.getIp(), bootstrapContact.getPort(), "...");

        // remember last successful bootstrap node
        Network._sendRPC(bootstrapContact, 'FIND_NODE', { id: Network._getHomeContact().getNodeId() })
            .then(function (res) {

                require('fs').writeFile(__dirname + "/../.lastBootstrap", ip + ':' + port, function(err) {});
                MeshMail.isBootstrapped = true;
                deferred.resolve(res)
            }, function (err) {
                deferred.reject(err);
            });

        return deferred.promise;
    },

    start: function (port) {

        var deferred = Q.defer();

        port = port || 0;

        Network.start(port).then(function (port) {

            console.log("Listening on port", port.toString().yellow);
            console.log("Your NodeID is", Network._getHomeContact().getNodeId());

            deferred.resolve(port);

        }, function (error) {
            deferred.reject(error);
        });

        return deferred.promise;
    },

    register: function (username, password) {

        var deferred = Q.defer();

        var publicProfile = new PublicProfile(username, MeshMail._generateSalt());
//        var privateProfile = new PrivateProfile(username);

        var networkKey = MeshMail._getHash('profile-' + username);

        Network.findValue(networkKey).then(function (res) {

            if (res) {
                console.log(res);

                deferred.reject(res);

            } else {
                Network.storeValue(
                    networkKey,
                    publicProfile
                );
                deferred.resolve(true);
            }
        });

        return deferred.promise;
    },

    getInbox: function (username) {
        var inboxKey = Util.getHash('inbox-' + username);
        return Network.findValue(inboxKey);
    },

    getMail: function (id) {
        return Network.findValue(id);
    },

    _getHash: function (string) {
        var md = forge.md.sha1.create();
        md.update(string);
        return md.digest().toHex();
    },

    _generateSalt: function (count) {
        count = count || 128;
        return forge.random.getBytesSync(count);
    },

    _deriveKey: function (password, salt) {
        salt = salt || Crypto.generateSalt();
        return forge.pkcs5.pbkdf2(password, salt, 1000, 16);
    }
};

module.exports = MeshMail;