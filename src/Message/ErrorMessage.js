"use strict";

var Util = require('../Util'),
    Q = require('q');

function ErrorMessage(method, data) {

    this.method = method;
    this.data = data;
    this.type = 'ERROR';

}

ErrorMessage.create = function (method, data) {

    var message = new ErrorMessage(method, data);

    return Util.randomID().then(function (randomID) {
        message.rpcID = randomID;
        return message;
    });
}

ErrorMessage.parseReceived = function (obj) {
    var message = new ErrorMessage(obj.method, obj.data);
    message.rpcID = obj.rpcID;
    return message;
}

module.exports = ErrorMessage;