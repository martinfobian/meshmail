"use strict";

var Util = require('../Util'),
    Q = require('q');

function RequestMessage(method, data) {

    this.method = method;
    this.data = data;
    this.type = 'REQUEST';
    this.try = 1;

}

RequestMessage.prototype.increaseTryCounter = function () {
  this.try += 1;
  return this;
};

RequestMessage.create = function (method, data) {

    var message = new RequestMessage(method, data);

    return Util.randomID().then(function (randomID) {
        message.rpcID = randomID;
        return message;
    });
}

RequestMessage.parseReceived = function (obj) {
    var message = new RequestMessage(obj.method, obj.data);
    message.rpcID = obj.rpcID;
    return message;
}

module.exports = RequestMessage;