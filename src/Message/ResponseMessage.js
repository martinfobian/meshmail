"use strict";

var Util = require('../Util'),
    Q = require('q');

function ResponseMessage(method, data) {

    this.method = method;
    this.data = data;
    this.type = 'RESPONSE';

}

ResponseMessage.create = function (requestMessage, data) {

    var message = new ResponseMessage(requestMessage.method, data);
    message.rpcID = requestMessage.rpcID;
    return message;
}

ResponseMessage.parseReceived = function (obj) {
    var message = new ResponseMessage(obj.method, obj.data);
    message.rpcID = obj.rpcID;
    return message;
}

module.exports = ResponseMessage;