"use strict";

var dgram = require('dgram'),
    Q = require('q'),
    Contact = require('./Contact'),
    Routing = require('./Routing'),
    NetworkStorage = require('./NetworkStorage'),
    _ = require('underscore'),
    util = require('util'),
    RequestMessage = require('./Message/RequestMessage'),
    ResponseMessage = require('./Message/ResponseMessage'),
    ErrorMessage = require('./Message/ErrorMessage'),
    Cache = require('./Cache');

var Network = {

    retries: 4,
    _alpha: 3,

    _sentRPCs: {},
    _lookups: {},

    findClosestNodes: function (hash) {
        return Network._lookupNode(hash, 'FIND_NODE');
    },

    findNode: function (hash) {
        return Network.findClosestNodes(hash);
    },

    findValue: function (hash) {

        var deferred = Q.defer();

        Network._lookupNode(hash, 'FIND_VALUE').then(function (res) {
            Cache.storeValue(hash, res);
            deferred.resolve(res);
        }, deferred.reject);

        return deferred.promise;
    },

    storeValue: function (hash, value) {

        var data = {
            id: hash,
            value: value
        };

        Network.findClosestNodes(hash).then(function (nodes) {
            _.each(nodes, function (contact) {
                Network._sendRPC(contact, 'STORE', data);
            });
        });
    },

    ping: function (contact) {
        return Network._sendRPC(contact, 'PING');
    },

    appendValue: function (hash, value) {

        console.log("AP", hash, value);

        var data = {
            id: hash,
            value: value
        };

        Network.findClosestNodes(hash).then(function (nodes) {
            console.log("N", nodes);
            _.each(nodes, function (contact) {
                Network._sendRPC(contact, 'APPEND', data);
            });
        }, console.error);
    },

    refreshValues: function () {
        NetworkStorage.all(function (data) {
            Network.storeValue(data.key, data.value);
        });
    },

    _lookupNode: function(lookupID, rpc) {

        rpc = rpc || 'FIND_NODE';

        var contactList = Routing.getClosestKnownNodes(lookupID);

        // we don't know of any other nodes and can return immediately
        if (contactList.length == 0) {
            var p = Q.defer();
            p.resolve(null);
            return p.promise;
        }

        Network._lookups[lookupID] = {
            lookupID: lookupID,
            contactList: contactList,
            activeContacts: _.pluck(contactList, 'nodeId'),
            alreadyContacted: [],
            activeProbes: [],
            deferred: Q.defer()
        };

        Network._lookupRound(lookupID, rpc);
        return Network._lookups[lookupID].deferred.promise;
    },

    _lookupRound: function (lookupID, rpc) {

        var chosen = Network._chooseNextNodes(lookupID);
        if (chosen.length > 0) {

            _.each(chosen, function (chosenNode) {

                // if the chosen node has not been contacted
                if (!Network._lookups[lookupID].alreadyContacted.containsID(chosenNode.nodeId)) {

                    Network._lookups[lookupID].alreadyContacted.pushUnique(chosenNode.nodeId);
                    Network._lookups[lookupID].activeProbes.pushUnique(chosenNode.nodeId);

                    Network._sendRPC(chosenNode, rpc, { id: lookupID })
                        .then(function (response) { // success

                            Network._lookups[lookupID].activeProbes.removeIDIfExists(chosenNode.nodeId);

                            // if we look for a value and if response contains the value, resolve immediately
                            if (rpc == 'FIND_VALUE' && response.data.value) {
                                Network._lookups[lookupID].deferred.resolve(response.data.value);
                            }

                            // add all nodes from response and add to
                            _.each(response.data.nodes, function (node) {
                                Network._lookups[lookupID].contactList.pushUnique(node);
                            });
                            Network._sortContacts(lookupID);

                            if (Network._lookups[lookupID].activeProbes.length < Network._alpha) {
                                Network._lookupRound(lookupID, rpc);
                            }

                        }, function (err) { // failure

                            Network._lookups[lookupID].activeProbes.removeIDIfExists(chosenNode.nodeId);
                            Network._lookups[lookupID].contactList.removeContactIfExists(chosenNode.nodeId);

                            if (Network._lookups[lookupID].activeProbes.length < Network._alpha) {
                                Network._lookupRound(lookupID, rpc);
                            }
                        });
                }
            });
        } else {

            if (Network._getUncontactedNodes(lookupID).length == 0
             && Network._lookups[lookupID].activeProbes.length == 0) {

                if (rpc == 'FIND_NODE') {
                    Network._sortContacts(lookupID);
                    Network._lookups[lookupID].deferred.resolve(
                        Network._lookups[lookupID].contactList.slice(0, Routing.K)
                    );
                } else {
                    Network._lookups[lookupID].deferred.resolve(null);
                }
            }
        }
    },

    _sortContacts: function (lookupID) {
        Network._lookups[lookupID].contactList = _.sortBy(Network._lookups[lookupID].contactList, function (contact) {
            return Routing.distance(contact.nodeId, lookupID);
        });
    },

    _getUncontactedNodes: function (lookupID) {
        return _.filter(Network._lookups[lookupID].contactList, function (contact) {
            return _.indexOf(Network._lookups[lookupID].alreadyContacted, contact.nodeId) == -1;
        });
    },

    _chooseNextNodes: function (lookupID) {
        // sort known nodes by distance
        Network._sortContacts(lookupID);

        return _.sample(
            Network._getUncontactedNodes(lookupID),
            Routing.K - Network._lookups[lookupID].activeProbes.length
        );
    },

    start: function (port) {

        var homeContact = Contact.create();
        var deferred = Q.defer();

        var _socket = dgram.createSocket('udp4');
        Network._socket = _socket;
        Network._homeContact = homeContact;

        _socket.on('listening', function () {
            Network._getHomeContact().setIp('127.0.0.1');
            Network._getHomeContact().setPort(_socket.address().port);
            this.port = _socket.address().port;
            Routing.init(Network);
            Network.startRefreshValuesLoop();
            deferred.resolve(_socket.address().port);
        });

        _socket.on('message', Routing.updateContacts);
        _socket.on('message', Network._handleIncomingMessage);
        _socket.bind(port || 0);

        return deferred.promise;
    },

    startRefreshValuesLoop: function () {
        setInterval(
            Network.refreshValues,
            1000 * 60 * 60
        ); // refresh every 60 minutes
    },

    stop: function () {
        Network._socket.stop();
    },

    _getHomeContact: function () {
        return Network._homeContact;
    },

    _getTimeout: function (requestMessage) {
        return Math.floor(400 * (Math.pow(2, requestMessage.try) - 1));
    },

    _sendRPCRequest: function (requestMessage, contact, rpcPromise) {

        var rpcRequestPromise = Q.defer();
        rpcRequestPromise
            .promise
            .timeout(Network._getTimeout(requestMessage))
            .then(function (response) {

                rpcPromise.resolve(response);
                delete Network._sentRPCs[requestMessage.rpcID];

            }, function (err) {

                if (err.message.indexOf('Timed out after') >= 0) {
                    if (requestMessage.try < Network.retries) {

                        requestMessage = requestMessage.increaseTryCounter();
                        Network._sendRPCRequest(requestMessage, contact, rpcPromise);

                    } else {

                        requestMessage.contact = contact;
                        rpcPromise.reject(err);

                        delete Network._sentRPCs[requestMessage.rpcID];
                    }
                }

            }).done();

        Network._sentRPCs[requestMessage.rpcID] = rpcRequestPromise;
        Network._send(contact, requestMessage);
    },

    _sendRPC: function (contact, method, data) {

        var deferred = Q.defer();

        RequestMessage.create(method, data).then(function (requestMessage) {

            Network._sendRPCRequest(requestMessage, contact, deferred);

        });

        return deferred.promise;
    },

    _send: function (contact, message) {

        message.senderID = Network._getHomeContact().getNodeId();
        var messageBuffer = new Buffer(JSON.stringify(message));

        Network._socket.send(
            messageBuffer,
            0,
            messageBuffer.length,
            contact.port,
            contact.ip
        );
    },

    _handleIncomingMessage: function (data, remote) {

        var decodedMessage = JSON.parse(data.toString());
        var contact = new Contact(decodedMessage.senderID, remote.address, remote.port);

        switch (decodedMessage.type) {
            case "REQUEST":
                Network._handleRequest(RequestMessage.parseReceived(decodedMessage), contact);
                break;
            case "RESPONSE":
                Network._handleResponse(ResponseMessage.parseReceived(decodedMessage), contact);
                break;
            case "ERROR":
                Network._handleError(ErrorMessage.parseReceived(decodedMessage), contact);
                break;
        }
    },

    _handleRequest: function (requestMessage, contact) {

        var data;

        switch (requestMessage.method) {
            case "FIND_NODE":
                data = { nodes: Routing.getClosestKnownNodes(requestMessage.data.id) };
                Network._sendResponse(requestMessage, contact, data);
                break;

            case "FIND_VALUE":

                NetworkStorage.getValue(requestMessage.data.id)
                    .then(function (value) {

                        if (value != null) {
                            data = { value: value }
                        } else {
                            data = { nodes: Routing.getClosestKnownNodes(requestMessage.data.id) };
                        }

                        Network._sendResponse(requestMessage, contact, data);
                    });
                break;

            case "PING":
                data = {};
                Network._sendResponse(requestMessage, contact, data);
                break;

            case "STORE":

                console.log("STORING", requestMessage.data.id);

                NetworkStorage.storeValue(requestMessage.data.id, requestMessage.data.value);
                data = {};
                Network._sendResponse(requestMessage, contact, data);
                break;

            case "APPEND":
                console.log("APPENDING", requestMessage.data.id);
                NetworkStorage.appendValue(requestMessage.data.id, requestMessage.data.value);
                data = {};
                Network._sendResponse(requestMessage, contact, data);
                break;

            case "PUSH":
                // resolve push notifications
                data = {};
                Network._sendResponse(requestMessage, contact, data);
                break;

        }
    },

    _sendResponse: function (requestMessage, contact, data) {
        if (data) {
            var respMessage = ResponseMessage.create(requestMessage, data);
            respMessage.rpcID = requestMessage.rpcID;
            Network._send(contact, respMessage);
        }
    },

    _handleResponse: function (responseMessage, contact) {

        if (_.indexOf(_.keys(Network._sentRPCs), responseMessage.rpcID) >= 0) {
            responseMessage.contact = contact;
            Network._sentRPCs[responseMessage.rpcID].resolve(responseMessage);
            delete Network._sentRPCs[responseMessage.rpcID];
        } else {
            console.log((new Date()).timeNow(), "Dropped Unrequested Response", util.inspect(responseMessage));
        }
    },

    _handleError: function (errorMessage, contact) {
        console.log('ErrM', require('util').inspect(errorMessage, null, 4));
    }
}

module.exports = Network;