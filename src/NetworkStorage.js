"use strict";

var levelup = require('levelup'),
    Util = require('./Util');
var db = levelup('/mesh-' + Util.randomIDSync(), { db: require('memdown'), valueEncoding: 'json' }),
    Q = require('q'),
    _ = require('underscore');

var NetworkStorage = {

    getValue: function (hash) {

        var deferred = Q.defer();

        db.get(hash, function (err, value) {

            if (err) {
                deferred.resolve(null);
            } else {
                deferred.resolve(value);
            }
        });

        return deferred.promise;
    },

    storeValue: function (hash, value) {

        var deferred = Q.defer();

        console.log("Storing", hash, value);

        db.put(hash, value, function (err) {

            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve({});
            }
        });

        return deferred.promise;
    },

    appendValue: function (hash, value) {

        var deferred = Q.defer();

        console.log("Appending", hash);

        NetworkStorage.getValue(hash).then(function (currentValue) {

            console.log("CUR", currentValue);

            if (currentValue == null) { // new value, just store
                NetworkStorage.storeValue(hash, [value]).then(deferred.resolve, deferred.reject);
            } else if (_.isArray(currentValue)) { // add value to existing array
                currentValue.push(value);
                NetworkStorage.storeValue(hash, currentValue).then(deferred.resolve, deferred.reject);
            } else { // make array from existing single value
                var newValue = [currentValue, value];
                NetworkStorage.storeValue(hash, newValue).then(deferred.resolve, deferred.reject);
            }

        });

        return deferred.promise;
    },

    all: function (dataFunction) {
        db.createReadStream().on('data', dataFunction);
    }
}

module.exports = NetworkStorage;