"use strict";

var KBucket = require('./KBucket'),
    Q = require('q'),
    Contact = require('./Contact'),
    _ = require('underscore');

var Routing = {
    K: 20,
    _buckets: [],
    _waitingQueues: [],

    Network: null,

    init: function (network) {
        Routing.Network = network;
        Routing.startBucketRefreshTimer();
    },

    getClosestKnownNodes: function (ID, limit) {

        limit = limit || Routing.K;

        var bucketIndex = Routing.getBucketIndex(ID);
        var originalIndex = bucketIndex;
        var result = [];

        while (bucketIndex >= 0 && result.length < limit) {

            result = result.concat(Routing.getBucket(bucketIndex).getContacts());
            bucketIndex -= 1;
        }

        // if still not enough, consider buckets farther away
        if (result.length < limit && originalIndex < 159) {
            bucketIndex = originalIndex + 1;
            while (bucketIndex < 160 && result.length < limit) {
                result = result.concat(Routing.getBucket(bucketIndex).getContacts());
                bucketIndex += 1;
            }
        }

        result = result.sort(function (a, b) {
            return Routing.distance(ID, a.getNodeId()) - Routing.distance(ID, b.getNodeId());
        });

        return result.slice(0, limit);
    },

    checkBucket: function (bucket) {
        // check first element and try to replace from waiting queue

        var oldestContact = bucket.getOld();
        return Routing.Network.ping(oldestContact);
    },

    addContact: function (contact) {

        if (contact.getNodeId() === Routing.Network._getHomeContact().getNodeId()) {
            return null;
        }

        var bucketIndex = Routing.getBucketIndex(contact.getNodeId());
        var bucket = Routing.getBucket(bucketIndex);

        try {
            bucket.addContact(contact);
        } catch (e) { // if bucket is full, add contact to waiting queue and trigger check bucket
            // create waiting queue for kbucket
            var queue = Routing.getWaitingQueue(bucketIndex);
            try {
                queue.addContact(contact);
                Routing.checkBucket(bucket).then(function () {
                    // ping successful, don't replace contact
                }, function () {
                    // contact unreachable, replace from waitingqueue
                    bucket._contacts.unshift(queue.popFresh());
                });
            } catch (f) {} // ignore waiting queue full errors
        }
    },

    getContact: function (nodeID) {
        return Routing.getBucket(Routing.getBucketIndex(nodeID)).getContact(nodeID);
    },

    getWaitingQueue: function (index) {
        if (!(index in Routing._waitingQueues)) {
            Routing._waitingQueues[index] = new KBucket(Math.pow(2, index), Math.pow(2, index + 1), Routing.K);
        }

        return Routing._buckets[index];
    },

    getBucket: function (index) {
        if (!Routing.bucketExists(index)) {
            Routing._buckets[index] = new KBucket(Math.pow(2, index), Math.pow(2, index + 1), Routing.K);
        }

        return Routing._buckets[index];
    },

    bucketExists: function (index) {
        return (index in Routing._buckets);
    },

    getBucketIndex: function (ID) {
        var dist = Routing.distance(ID, Routing.Network._getHomeContact().getNodeId());
        if (dist == 0) {
            return 0;
        } else {
            return Math.floor(Math.log(dist) / Math.log(2));
        }
    },

    startBucketRefreshTimer: function () {

        setInterval(function () {
            for (var i = 0; i < 160; i += 1) {
                var bucket = Routing.getBucket(i);
                bucket.touch(Routing.Network.findNode);
            }
        }, 1000 * 60 * 10); // refresh every 10 minutes
    },

    distance: function (ID1, ID2) {
        return parseInt(Routing._xor(Routing._hexToBinary(ID1), Routing._hexToBinary(ID2)), 2);
    },

    updateContacts: function (data, remote) {
        var decodedMessage = JSON.parse(data.toString());
        var contact = new Contact(decodedMessage.senderID, remote.address, remote.port);

        Routing.addContact(contact);
//        Routing.touchContact(contact);
    },

//    printBuckets: function () {
//        _.each(Routing._buckets, function (bucket) {
//            console.log("BUCKET", Routing.getBucketIndex(bucket._rangeMin), require('util').inspect(bucket.getContacts()));
//        });
//
//    },

    touchBucket: function (ID) {
//        console.log("Touching", contact.getNodeId());
        Routing.getBucket(Routing.getBucketIndex(ID)).touch();
    },

    _hexToBinary: function (s) {
        var i, k, part, ret = '';
        // lookup table for easier conversion. '0' characters are padded for '1' to '7'
        var lookupTable = {
            '0': '0000', '1': '0001', '2': '0010', '3': '0011', '4': '0100',
            '5': '0101', '6': '0110', '7': '0111', '8': '1000', '9': '1001',
            'a': '1010', 'b': '1011', 'c': '1100', 'd': '1101',
            'e': '1110', 'f': '1111',
            'A': '1010', 'B': '1011', 'C': '1100', 'D': '1101',
            'E': '1110', 'F': '1111'
        };
        for (i = 0; i < s.length; i += 1) {
            if (lookupTable.hasOwnProperty(s[i])) {
                ret += lookupTable[s[i]];
            } else {
                return { valid: false };
            }
        }
        return ret;
    },

    _xor: function (nodeID1, nodeID2) {
        var xor = "";

        for (var i = 0; i < nodeID1.length && i < nodeID2.length; ++i) {
            xor += parseInt(nodeID1.charAt(i)) ^ parseInt(nodeID2.charAt(i)).toString(2);
        }
        return xor;
    },

    /**
     * Ping
     * @param contact
     */
    replaceOldest: function (contact) {

    }
};

module.exports = Routing;