"use strict";

var Q = require('q'),
    forge = require('node-forge');

var Util = Util || {};

Util.getRandomString = function (lengthInBytes) {
    var deferred = Q.defer();
    require('crypto').randomBytes(lengthInBytes, function(ex, buf) {
        if (ex) {
            deferred.reject(ex);
        }
        deferred.resolve(buf.toString('hex'));
    });

    return deferred.promise;
};

Util.randomID = function () {
    return Util.getRandomString(20);
}

Util.randomIDSync = function () {
    var bytes = forge.random.getBytesSync(20);
    return forge.util.bytesToHex(bytes);
}

Util.getHash = function (string) {
    var md = forge.md.sha1.create();
    md.update(string);
    return md.digest().toHex();
}

module.exports = Util;