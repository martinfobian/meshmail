'use strict';

var express = require('express');
var http = require('http'),
    path = require('path'),
    flash = require('connect-flash'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    MeshMail = require('./MeshMail');

var webUI = {

    meshMail: MeshMail,

    requiredAuthentication: function (req, res, next) {

        if (req.isAuthenticated()) {
            next();
        } else {
            req.session.error = 'Access denied!';
            res.redirect('/login');
        }
    },

    requiredBootstrapping: function (req, res, next) {
        if (MeshMail.isBootstrapped) {
            next();
        } else {
            res.redirect('/bootstrap');
        }
    },

    start: function (port) {

        port = port || 0;

        var app = express();
        var server = http.createServer(app).listen();
        app.set('port', server.address().port);

        var swig = require('swig');
        app.engine('html', swig.renderFile);
        app.set('views', __dirname + '/../views');
        app.use(express.static(__dirname + '/../assets'));
        app.set('view engine', 'html');

        app.use(express.bodyParser());
        app.use(express.cookieParser() );
        app.use(express.session({ secret: 'keyboard cat' }));
        app.use(passport.initialize());
        app.use(passport.session());
        app.use(flash());

        passport.use(new LocalStrategy(
            function(username, password, done) {

                console.log("USER", username);
                console.log("PW", password);

                return done(null, { username: username, loggedIn: true });

// @TODO: This will allow all username/password combinations at the moment
//                if (username == 'mail@martinfobian.de' && password == 'krusl21') {
//                    return done(null, { username: username, loggedIn: true });
//                } else {
//                    return done(null, false, { message: 'Invalid credentials!' });
//                }
            }
        ));

        passport.serializeUser(function (user, done) {
            done(null, user.username);
        });

        passport.deserializeUser(function (username, done) {
            done(null, { username: username });
        });

        // support flash messages
        app.use(function(req, res, next) {
            res.locals.flash = function() {
                return req.flash()
            };
            next();
        });

        // Routing (read controller dir)
        require('fs').readdirSync(__dirname + '/../controller/').forEach(function(name) {
                var route = require(__dirname + '/../controller/' + name);
                route(app);
            });


        require('fs').writeFile(__dirname + "/../.lastWebPort", app.get('port'), function(err) {});
        return app.get('port');
    }
}

module.exports = webUI;