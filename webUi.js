var fs = require('fs'),
    open = require('open');

fs.readFile(__dirname + '/.lastWebPort', {encoding: 'utf-8'}, function(err, data){
    if (!err){
        open('http://localhost:' + data);
    }else{
        console.log(err);
    }
});